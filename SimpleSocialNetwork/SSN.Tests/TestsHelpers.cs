﻿using Microsoft.EntityFrameworkCore;
using SSN.DAL;
using SSN.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SSN.Tests
{
    public class TestsHelpers
    {
        public static DbContextOptions<SsnContext> ContextOptions(string ConnectionString)
        {
            DbContextOptionsBuilder<SsnContext> optionsBuilder = new DbContextOptionsBuilder<SsnContext>();
            DbContextOptions<SsnContext> options = optionsBuilder
                .UseSqlServer(ConnectionString)
                .Options;
            return options;
        }

        public static IEnumerable<Country> InitialCountries =>
        new[]
        {
            new Country { Id = 1, CountryName = "Ukraine", ActiveDataState = true },
            new Country { Id = 2, CountryName = "USA", ActiveDataState = true }
        };

        public static IEnumerable<City> InitialCities =>
        new[]
        {
            new City { Id = 1, CityName = "Kyiv", CountryId = 1, ActiveDataState = true },
            new City { Id = 2, CityName = "Lviv", CountryId = 1, ActiveDataState = true },
            new City { Id = 3, CityName = "Odesa", CountryId = 1, ActiveDataState = true },
            new City { Id = 4, CityName = "Dnipro", CountryId = 1, ActiveDataState = true },
            new City { Id = 5, CityName = "Kharkiv", CountryId = 1, ActiveDataState = true },

            new City { Id = 6, CityName = "Washington", CountryId = 2, ActiveDataState = true },
            new City { Id = 7, CityName = "New York", CountryId = 2, ActiveDataState = true },
            new City { Id = 8, CityName = "San Francisko", CountryId = 2, ActiveDataState = true },
            new City { Id = 9, CityName = "Chicago", CountryId = 2, ActiveDataState = true },
            new City { Id = 10, CityName = "Dallas", CountryId = 2, ActiveDataState = true }
        };

        public static IEnumerable<UserRole> InitialUserRoles =>
        new[]
        {
            new UserRole { Id = 1, RoleName = "Guest", ActiveDataState = true },
            new UserRole { Id = 2, RoleName = "User", ActiveDataState = true },
            new UserRole { Id = 3, RoleName = "Admin", ActiveDataState = true }
        };

        public static IEnumerable<User> InitialUsers =>
        new[]
        {
            new User { Id = 1, FirstName = "FirstName1", LastName = "LastName1", Login = "Login1", Password = "Password1", Birthday = new DateTime(2001, 01, 01), UserRoleId = 2, CityId = 1, ActiveDataState = true },
            new User { Id = 2, FirstName = "FirstName2", LastName = "LastName2", Login = "Login2", Password = "Password2", Birthday = new DateTime(2002, 02, 02), UserRoleId = 2, CityId = 2, ActiveDataState = true },
            new User { Id = 3, FirstName = "FirstName3", LastName = "LastName3", Login = "Login3", Password = "Password3", Birthday = new DateTime(2003, 03, 03), UserRoleId = 2, CityId = 3, ActiveDataState = true }
        };





    }
}
