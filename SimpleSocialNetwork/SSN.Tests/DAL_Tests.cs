using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using SSN.DAL;

namespace SSN.Tests
{
    public class DAL_Tests
    {
        private string _connectionString;
        private DbContextOptions<SsnContext> _options;

        [SetUp]
        public void Setup()
        {
            _connectionString = "Data Source=(LocalDB)\\MSSQLLocalDB; AttachDbFilename = C:\\Users\\Pvl\\SimpleSocialNetworkDB.mdf; Integrated Security=True";
            //_connectionString = "Data Source=(LocalDB)\\MSSQLLocalDB; AttachDbFilename = SimpleSocialNetworkDB.mdf; Integrated Security=True";
            //_connectionString = "Data Source=(LocalDB)\\MSSQLLocalDB; AttachDbFilename = SimpleSocialNetworkDB2.mdf; Integrated Security=True"; 
            _options = TestsHelpers.ContextOptions(_connectionString);

        }

        [Test]
        public void SsnDal_CreateSSNContextObject_NotNull()
        {
            //act
            SsnContext sSNContext;

            //arrange
            sSNContext = new SsnContext(_options);

            //assert
            Assert.AreNotEqual(null, sSNContext);
        }
    }
}