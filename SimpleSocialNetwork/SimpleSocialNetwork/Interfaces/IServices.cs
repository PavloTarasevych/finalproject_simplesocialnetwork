﻿using AutoMapper;
using SSN.BL.Interfaces;
using SSN.BL.Models;

namespace SimpleSocialNetworkPL.Interfaces
{
    public interface IServices
    {
        UserModel Authenticate(string login, string password);
        ICityService ICityService { get; }
        ICountryService ICountryService { get; }
        IMessageService IMessageService { get; }
        IPostService IPostService { get; }
        IUserService IUserService { get; }
        IUserRoleService IUserRoleService { get; }
        IMapper IMapper { get; }
    }
}
