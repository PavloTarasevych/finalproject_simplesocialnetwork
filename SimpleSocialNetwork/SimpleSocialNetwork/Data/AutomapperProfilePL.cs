﻿using AutoMapper;
using SSN.BL.Models;
using SSN.PL.Models;

namespace SSN.PL
{
    public class AutomapperProfilePL : Profile
    {
        public AutomapperProfilePL()
        {
            CreateMap<RegisterModel, UserModel>()
                .ForMember(um => um.FirstName, opt => opt.MapFrom(rm => rm.FirstName))
                .ForMember(um => um.LastName, opt => opt.MapFrom(rm => rm.LastName))
                .ForMember(um => um.Login, opt => opt.MapFrom(rm => rm.Login))
                .ForMember(um => um.Password, opt => opt.MapFrom(rm => rm.Password))
                .ForMember(um => um.Birthday, opt => opt.MapFrom(rm => rm.Birthday))
                .ForMember(um => um.CityIds, opt => opt.MapFrom(rm => rm.CityId))
                .ReverseMap();

            CreateMap<UserView, UserModel>()
                .ForMember(um => um.Id, opt => opt.MapFrom(uv => uv.Id))
                .ForMember(um => um.FirstName, opt => opt.MapFrom(uv => uv.FirstName))
                .ForMember(um => um.LastName, opt => opt.MapFrom(uv => uv.LastName))
                .ForMember(um => um.Login, opt => opt.MapFrom(uv => uv.Login))
                .ForMember(um => um.Password, opt => opt.MapFrom(uv => uv.Password))
                .ForMember(um => um.Birthday, opt => opt.MapFrom(uv => uv.Birthday))
                .ForMember(um => um.UserRoleIds, opt => opt.MapFrom(uv => uv.UserRoleIds))
                .ForMember(um => um.CityIds, opt => opt.MapFrom(uv => uv.CityIds))
                .ForMember(um => um.PostsIds, opt => opt.MapFrom(uv => uv.PostsIds))
                .ForMember(um => um.MessagesIds, opt => opt.MapFrom(uv => uv.MessagesIds))
                .ForMember(um => um.FriendsIds, opt => opt.MapFrom(uv => uv.FriendsIds))
                .ReverseMap();

        }

    }
}
