﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using SimpleSocialNetworkPL.Interfaces;
using SSN.BL;
using SSN.BL.Interfaces;
using SSN.BL.Models;
using SSN.DAL.Entities;
using SSN.PL;
using System;
using System.IO;
using System.Linq;

namespace SimpleSocialNetwork.Data
{
    /// <summary>
    /// Database operations class
    /// </summary>
    public class ServicesOpeartion : IServices
    {
        private readonly IFacadeServices _allServices;
        private readonly IMapper _iMapperPL;

        public ServicesOpeartion()
        {
            ConnectionString = GetConnString();
            if (null == _allServices)
            {
                _allServices = new FacadeServices(ConnectionString);
            }
            if (null == _iMapperPL)
            {
                _iMapperPL = CreateIMapperPL();
            }
        }

        public string ConnectionString { get; }

        private string GetConnString()
        {
                ConfigurationBuilder builder = new ConfigurationBuilder();
                builder.SetBasePath(Directory.GetCurrentDirectory());
                builder.AddJsonFile("appsettings.json");
                IConfigurationRoot config = builder.Build();
                return config.GetConnectionString("SimpleSocialNetworkDB_connection");
        }

        public UserModel Authenticate(string login, string password)
        {
            if (string.IsNullOrWhiteSpace(login) || string.IsNullOrWhiteSpace(password))
                return null;

            UserModel userModel = this.IUserService.GetAll().FirstOrDefault(um => um.Login == login);

            if (null == userModel)
                return null;

            if (!VerifyPasswordHash(password))
                return null;

            return userModel;
        }

        private static bool VerifyPasswordHash(string password)
        {
            if (null == password) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");
            return true;
        }

        private IMapper CreateIMapperPL()
        {
            var myProfile = new SSN.PL.AutomapperProfilePL();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));

            return configuration.CreateMapper();
        }

        public ICityService ICityService
        {
            get 
            { 
                return _allServices.CityService;
            }
        }

        public IMessageService IMessageService
        {
            get
            {
                return _allServices.MessageService;
            }
        }


        public IPostService IPostService
        {
            get
            {
                return _allServices.PostService;
            }
        }

        public IUserService IUserService
        {
            get
            {
                return _allServices.UserService;
            }
        }

        public IUserRoleService IUserRoleService
        {
            get
            {
                return _allServices.UserRoleService;
            }
        }

        public ICountryService ICountryService
        {
            get
            {
                return _allServices.CountryService;
            }
        }

        public IMapper IMapper => _iMapperPL;

    }
}
