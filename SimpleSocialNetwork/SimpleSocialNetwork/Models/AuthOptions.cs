﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace SimpleSocialNetworkPL.Models
{
    public class AuthOptions
    {
        public readonly string Issuer;
        public readonly string Audience;
        private readonly string _Key;
        public readonly int Lifetime;

        public AuthOptions(IConfiguration iConfig)
        {
            Issuer = iConfig.GetSection("SSNSettings").GetSection("Issuer").Value;
            Audience = iConfig.GetSection("SSNSettings").GetSection("Audience").Value;
            _Key = iConfig.GetSection("SSNSettings").GetSection("JWTSecret").Value;
            Lifetime = int.Parse(iConfig.GetSection("SSNSettings").GetSection("Lifetime").Value);
        }

        public SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_Key));
        }
    }
}
