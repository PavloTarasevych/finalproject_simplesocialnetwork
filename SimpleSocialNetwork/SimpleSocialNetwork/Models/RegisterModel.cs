﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SSN.PL.Models
{
    public class RegisterModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 1, ErrorMessage = "{0} length must be between {2} and {1}.")]
        public string Login { get; set; }

        [Required]
        [StringLength(40, MinimumLength = 5, ErrorMessage = "{0} length must be between {2} and {1}.")]
        [RegularExpression(@"(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#/*-+]).{5,40}")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public DateTime Birthday { get; set; }

        public int CityId { get; set; }
    }
}
