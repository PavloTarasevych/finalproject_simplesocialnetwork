﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using SimpleSocialNetworkPL.Interfaces;
using SimpleSocialNetworkPL.Models;
using SSN.BL.Models;
using SSN.DAL;
using SSN.PL;
using SSN.PL.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace SimpleSocialNetworkPL.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IServices _iServices;
        private readonly IMapper _iMapper;
        private readonly IConfiguration _configuration;
        private readonly AuthOptions _authOptions;

        public UsersController(IServices iServices,
            IConfiguration iConfig)
        {
            _iServices = iServices;

            var myProfile = new AutomapperProfilePL();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            _iMapper = configuration.CreateMapper();

            _configuration = iConfig;
            _authOptions = new AuthOptions(_configuration);
        }

        ////https://jasonwatmore.com/post/2019/10/14/aspnet-core-3-simple-api-for-authentication-registration-and-user-management#authenticate-model-cs
        //[AllowAnonymous]
        //[HttpPost("Authenticate")]
        //public IActionResult Authenticate([FromBody] AuthenticateModel model)
        //{
        //    UserModel userModel = _iServices.IUserService.Authenticate(model.Login, model.Password);

        //    if (userModel == null)
        //        return BadRequest(new { message = "Username or password is incorrect" });

        //    var tokenHandler = new JwtSecurityTokenHandler();
        //    var key = Encoding.ASCII.GetBytes(_configuration.GetSection("SSNSettings").GetSection("JWTSecret").Value);
        //    var tokenDescriptor = new SecurityTokenDescriptor
        //    {
        //        Subject = new ClaimsIdentity(new Claim[]
        //        {
        //            new Claim(ClaimTypes.Name, userModel.Id.ToString())
        //        }),
        //        Expires = DateTime.UtcNow.AddDays(7),
        //        SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
        //    };
        //    UserView userView = _iMapper.Map<UserModel, UserView>(userModel);
        //    SecurityToken securityToken = tokenHandler.CreateToken(tokenDescriptor);
        //    string tokenString = tokenHandler.WriteToken(securityToken);

        //    return Ok(new {
        //        Id = userView.Id,
        //        Login = userView.Login,
        //        FirstName = userView.FirstName,
        //        LastName = userView.LastName,
        //        Token = tokenString
        //    });
        //}

        [AllowAnonymous]
        [HttpPost("Authenticate")]
        public IActionResult Authenticate([FromBody] AuthenticateModel model)
        {
            var identity = GetIdentity(model);
            if (identity == null)
            {
                return BadRequest(new { errorText = "Invalid username or password." });
            }

            var now = DateTime.UtcNow;
            var jwt = new JwtSecurityToken(
                    issuer: _authOptions.Issuer,
                    audience: _authOptions.Audience,
                    notBefore: now,
                    claims: identity.Claims,
                    expires: now.Add(TimeSpan.FromDays(_authOptions.Lifetime)),
                    signingCredentials: new SigningCredentials(_authOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            var response = new
            {
                username = identity.Name,
                access_token = encodedJwt
            };
            return Ok(response);
        }

        private ClaimsIdentity GetIdentity(AuthenticateModel model)
        {
            UserModel userModel = _iServices.IUserService.Authenticate(model.Login, model.Password);
            if (null != userModel)
            {
                string userRoleName = _iServices.IUserRoleService.GetAll().FirstOrDefault(urm => urm.Id == userModel.UserRoleIds).RoleName;
                var claims = new List<Claim>
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, userModel.Login),
                    new Claim(ClaimsIdentity.DefaultRoleClaimType, userRoleName)
                };
                ClaimsIdentity claimsIdentity =
                new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                    ClaimsIdentity.DefaultRoleClaimType);
                return claimsIdentity;
            }
            return null;
        }

        [AllowAnonymous]
        [HttpPost("Register")]
        public IActionResult Register([FromBody] RegisterModel model)
        {
            UserModel userModel = _iMapper.Map<UserModel>(model);
            try
            {
                bool result = null == _iServices.IUserService.GetAll().FirstOrDefault(um => um.Login == model.Login);
                if (result)
                {
                    _iServices.IUserService.AddAsync(userModel).Wait();
                    return Ok();
                }
                else
                {
                    return BadRequest("Login alredy exist.");
                }
            }
            catch (SSNException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }


        [AllowAnonymous]
        [HttpPost("Errors")]
        public IActionResult Errors()
        {
            return Ok("Happens something wrong.");
        }


        [AllowAnonymous]
        [HttpPost("Forbidden")]
        public IActionResult Forbidden()
        {
            return Ok("Access forbidden.");
        }


        [Authorize(Roles = "Admin")]
        [HttpGet]
        public IActionResult GetUsers()
        {
            var allUsers = _iServices.IUserService.GetAll();
            return Ok(allUsers);
        }

        [Authorize(Roles = "Admin")]
        [HttpGet("Roles")]
        public IActionResult GetRoles()
        {
            IEnumerable<UserRoleModel> allRoles = _iServices.IUserRoleService.GetAll();
            return Ok(allRoles);
        }

        [Authorize(Roles = "Admin")]
        [HttpGet("Countries")]
        public async Task<IEnumerable<CountryModel>> GetAllCountries()
        {
            return  await Task.Run(() => _iServices.ICountryService.GetAll());
        }

        [Authorize(Roles = "Admin")]
        [HttpGet("Countries/{countryId}")]
        public IActionResult GetCountry(int countryId)
        {
            if(0 > countryId)   return NotFound();
            CountryModel countryModel = _iServices.ICountryService.GetByIdAsync(countryId).Result;
            if (null != countryModel)
            {
                return Ok(countryModel);
            }
            return NotFound();
        }

        [Authorize(Roles = "Admin")]
        [HttpGet("{userId}")]
        public async Task<ActionResult<UserModel>> GetUser(int userId)
        {
            UserModel user = await _iServices.IUserService.GetByIdAsync(userId);
            if (null == user)
            {
                return NotFound();
            }
            else
            {
                return user;
            }
        }

        [HttpGet("{userId}/Posts")]
        public async Task<IEnumerable<PostModel>> GetUsersPosts(int userId)
        {
            UserModel user = await _iServices.IUserService.GetByIdAsync(userId);
            IEnumerable<PostModel> allPosts = _iServices.IPostService.GetAll();
            IEnumerable<PostModel> needPosts = await Task.Run(() => allPosts.Join(user.PostsIds, p => p.Id, i => i, (p, i) => p));
            return needPosts;
        }

        [HttpGet("{userId}/Messages")]
        public async Task<IEnumerable<MessageModel>> GetUsersMessages(int userId)
        {
            UserModel user = await _iServices.IUserService.GetByIdAsync(userId);
            IEnumerable<MessageModel> allMessages = _iServices.IMessageService.GetAll();
            IEnumerable<MessageModel> needMessages = await Task.Run(() => allMessages.Join(user.MessagesIds, m => m.Id, i => i, (m, i) => m));
            return needMessages;
        }



        //  POST-create
        //  PUT-update
        //  DELETE-remove



        //POST user
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult PostUser([FromBody] UserModel userModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Wrong user data");
            }
            _iServices.IUserService.AddAsync(userModel);
            return Ok();
        }

        //POST userrole
        [Authorize(Roles = "Admin")]
        [HttpPost("Roles")]
        public ActionResult PostUserRole([FromBody] UserRoleModel userRoleModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Wrong user role data");
            }
            _iServices.IUserRoleService.AddAsync(userRoleModel);
            return Ok();
        }

        //POST post
        [HttpPost("{id}/Post")]
        public ActionResult PostUserPost([FromBody] PostModel postModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Wrong UserPost data");
            }
            _iServices.IPostService.AddAsync(postModel);
            return Ok();
        }

        //POST message
        [HttpPost("{id}/Messages")]
        public ActionResult PostUserMessage([FromBody] MessageModel messageModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Wrong UserMessage data");
            }
            _iServices.IMessageService.AddAsync(messageModel);
            return Ok();
        }

        //POST country
        [Authorize(Roles = "Admin")]
        [HttpPost("{id}/Countries")]
        public ActionResult PostCountry([FromBody] CountryModel countryModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Wrong CountryModel data");
            }
            _iServices.ICountryService.AddAsync(countryModel);
            return Ok();
        }

        //POST city
        [Authorize(Roles = "Admin")]
        [HttpPost("{id}/Messages")]
        public ActionResult PostCity([FromBody] MessageModel messageModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Wrong MessageModel data");
            }
            _iServices.IMessageService.AddAsync(messageModel);
            return Ok();
        }



        ////PUT user
        //[Authorize(Roles = "Admin, User")]
        //[HttpPut]
        //public ActionResult PutUser([FromBody] UserModel userModel)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest("Wrong user data");
        //    }
        //    _iServices.IUserService.UpdateAsync(userModel);
        //    return Ok();
        //}

        ////PUT userrole
        //[Authorize(Roles = "Admin")]
        //[HttpPut]
        //public ActionResult PutUserRole([FromBody] UserRoleModel userRoleModel)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest("Wrong user role data");
        //    }
        //    _iServices.IUserRoleService.UpdateAsync(userRoleModel);
        //    return Ok();
        //}

        ////PUT post
        //[HttpPut("{id}/Post")]
        //public ActionResult PutUserPost([FromBody] PostModel postModel)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest("Wrong UserPost data");
        //    }
        //    _iServices.IPostService.UpdateAsync(postModel);
        //    return Ok();
        //}

        ////PUT message
        //[HttpPut("{id}/Messages")]
        //public ActionResult PutUserMessage([FromBody] MessageModel messageModel)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest("Wrong UserMessage data");
        //    }
        //    _iServices.IMessageService.UpdateAsync(messageModel);
        //    return Ok();
        //}

        ////PUT country
        //[Authorize(Roles = "Admin")]
        //[HttpPut("{id}/Countries")]
        //public ActionResult PutCountry([FromBody] CountryModel countryModel)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest("Wrong CountryModel data");
        //    }
        //    _iServices.ICountryService.UpdateAsync(countryModel);
        //    return Ok();
        //}

        ////PUT city
        //[Authorize(Roles = "Admin")]
        //[HttpPut("{id}/Messages")]
        //public ActionResult PutCity([FromBody] CityModel cityModel)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest("Wrong MessageModel data");
        //    }
        //    _iServices.ICityService.UpdateAsync(cityModel);
        //    return Ok();
        //}


        //// DELETE user
        //[HttpDelete]
        //public ActionResult DeleteUser([FromBody] UserModel userModel)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest("Wrong user data");
        //    }
        //    _iServices.IUserService.DeleteByIdAsync(userModel.Id);
        //    return Ok();
        //}

        //// DELETE userrole
        //[Authorize(Roles = "Admin")]
        //[HttpDelete]
        //public ActionResult DeleteUserRole([FromBody] UserRoleModel userRoleModel)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest("Wrong user role data");
        //    }
        //    _iServices.IUserRoleService.DeleteByIdAsync(userRoleModel.Id);
        //    return Ok();
        //}

        //// DELETE post
        //[HttpDelete("{id}/Post")]
        //public ActionResult DeleteUserPost([FromBody] PostModel postModel)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest("Wrong UserPost data");
        //    }
        //    _iServices.IPostService.DeleteByIdAsync(postModel.Id);
        //    return Ok();
        //}

        //// DELETE message
        //[HttpDelete("{id}/Messages")]
        //public ActionResult DeleteUserMessage([FromBody] MessageModel messageModel)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest("Wrong UserMessage data");
        //    }
        //    _iServices.IMessageService.DeleteByIdAsync(messageModel.Id);
        //    return Ok();
        //}

        //// DELETE  country
        //[Authorize(Roles = "Admin")]
        //[HttpDelete("{id}/Countries")]
        //public ActionResult DeleteCountry([FromBody] CountryModel countryModel)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest("Wrong CountryModel data");
        //    }
        //    _iServices.ICountryService.DeleteByIdAsync(countryModel.Id);
        //    return Ok();
        //}

        //// DELETE city
        //[Authorize(Roles = "Admin, User")]
        //[HttpDelete("{id}/Messages")]
        //public ActionResult DeleteCity([FromBody] CityModel cityModel)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest("Wrong MessageModel data");
        //    }
        //    _iServices.ICityService.DeleteByIdAsync(cityModel.Id);
        //    return Ok();
        //}



        ////[AcceptVerbs("GET", "POST")]
        ////public IActionResult HasLogin(string login)
        ////{
        ////    if (!_servicesOpeartion.IUserService.HasLogin(login))
        ////    {
        ////        return NotFound(login);
        ////    }
        ////    return Ok();
        ////}

        ////[HttpPost]
        ////public IActionResult VerifyLoginPassword(string login, string password)
        ////{
        ////    if (!_servicesOpeartion.IUserService.VerifyLoginPassword(login, password))
        ////    {
        ////        return NotFound();
        ////    }
        ////    return Ok();
        ////}


    }
}
