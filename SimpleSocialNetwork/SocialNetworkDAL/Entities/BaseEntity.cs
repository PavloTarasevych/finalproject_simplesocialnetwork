﻿namespace SSN.DAL.Entities
{
    public class BaseEntity
    {
        public int Id { get; set; }
        public bool ActiveDataState { get; set; }
    }
}
