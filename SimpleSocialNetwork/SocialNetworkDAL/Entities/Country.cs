﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SSN.DAL.Entities
{
    public class Country : BaseEntity
    {
        [Required]
        public string CountryName { get; set; }

        public virtual ICollection<City> Cities { get; set; }
    }
}
