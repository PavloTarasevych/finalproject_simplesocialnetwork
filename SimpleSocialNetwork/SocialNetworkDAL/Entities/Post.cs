﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SSN.DAL.Entities
{
    public class Post : BaseEntity
    {
        [Required]
        public string Title { get; set; }
        [Required]
        public string Content { get; set; }
        public DateTime Published { get; set; }


        public virtual int UserIds { get; set; }
    }
}
