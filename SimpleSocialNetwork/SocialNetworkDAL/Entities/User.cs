﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SSN.DAL.Entities
{
    public class User : BaseEntity
    {
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }

        [Required]
        public string Login { get; set; }
        [Required]
        public string Password { get; set; }

        public DateTime Birthday { get; set; }

        public virtual int UserRoleId { get; set; }
        public virtual int CityId { get; set; }
        public virtual ICollection<Post> Posts { get; set; }
        public virtual ICollection<Message> Messages { get; set; }
        public virtual ICollection<User> Friends { get; set; }
    }
}
