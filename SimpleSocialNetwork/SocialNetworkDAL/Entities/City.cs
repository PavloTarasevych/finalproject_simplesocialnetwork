﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SSN.DAL.Entities
{
    public class City : BaseEntity
    {
        [Required]
        public string CityName { get; set; }

        public virtual int CountryId { get; set; }
        public virtual Country Country { get; set; }
    }
}
