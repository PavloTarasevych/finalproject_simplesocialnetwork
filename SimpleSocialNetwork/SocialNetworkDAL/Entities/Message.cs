﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SSN.DAL.Entities
{
    public class Message : BaseEntity
    {
        [Required]
        public string Content { get; set; }
        public DateTime DateTimeSent { get; set; }

        [Required]
        public int OwnerUserId { get; set; }
        [Required]
        public int ToUserId { get; set; }
    }
}
