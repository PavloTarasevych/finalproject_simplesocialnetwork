﻿using System.ComponentModel.DataAnnotations;

namespace SSN.DAL.Entities
{
    public class UserRole : BaseEntity
    {
        [Required]
        public string RoleName { get; set; }
    }
}
