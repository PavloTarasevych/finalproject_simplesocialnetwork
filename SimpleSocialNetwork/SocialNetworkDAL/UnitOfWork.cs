using SSN.DAL.Entities;
using SSN.DAL.Interfaces;
using SSN.DAL.Repositories;
using System;
using System.Threading.Tasks;

namespace SSN.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly SsnContext _ssnDbContext;

        private CityRepository      _cityRepository;
        private CountryRepository   _countryRepository;
        private MessageRepository   _messageRepository;
        private PostRepository      _postRepository;
        private UserRepository      _userRepository;
        private UserRoleRepository  _userRoleRepository;

        public UnitOfWork(SsnContext ssnDbContext)
        {
            _ssnDbContext = ssnDbContext ?? throw new ArgumentNullException("UnitOfWork", "ssnDbContext is null.");
        }

        public IRepository<City> CitiesRepository
        {
            get
            {
                if (null == _cityRepository)
                {
                    _cityRepository = new CityRepository(_ssnDbContext);
                }
                return _cityRepository;
            }
        }

        public IRepository<Country> CountriesRepository
        {
            get
            {
                if (null == _countryRepository)
                {
                    _countryRepository = new CountryRepository(_ssnDbContext);
                }
                return _countryRepository;
            }
        }

        public IRepository<Message> MessagesRepository
        { 
            get
            {
                if (null == _messageRepository)
                {
                    _messageRepository = new MessageRepository(_ssnDbContext);
                }
                return _messageRepository;
            }
        }

        public IRepository<Post> PostsRepository
        { 
            get
            {
                if (null == _postRepository)
                {
                    _postRepository = new PostRepository(_ssnDbContext);
                }
                return _postRepository;
            }
        }

        public IRepository<User> UsersRepository
        {
            get
            {
                if (null == _userRepository)
                {
                    _userRepository = new UserRepository(_ssnDbContext);
                }
                return _userRepository;
            }
        }

        public IRepository<UserRole> UserRolesRepository
        {
            get
            {
                if (null == _userRoleRepository)
                {
                    _userRoleRepository = new UserRoleRepository(_ssnDbContext);
                }
                return _userRoleRepository;
            }
        }

        public void Dispose()
        {
            _cityRepository = null;
            _countryRepository = null;
            _messageRepository = null;
            _postRepository = null;
            _userRepository = null;
            _userRoleRepository = null;
        }

        Task IUnitOfWork.SaveAsync()
        {
            return _ssnDbContext.SaveChangesAsync();
        }
    }
}