﻿using Microsoft.EntityFrameworkCore;
using SSN.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SSN.DAL
{
    public static class SsnContextInitializer
    {
        /// <summary>
        /// Set initial data in database
        /// </summary>
        /// <param name="modelBuilder"></param>
        public static void Seed(this ModelBuilder modelBuilder)
        {
            
            modelBuilder.Entity<Country>().HasData(
                new Country[]
                {
                    new Country { Id = 1, CountryName = "Ukraine", ActiveDataState = true },
                    new Country { Id = 2, CountryName = "USA", ActiveDataState = true }
                });
            
            modelBuilder.Entity<City>().HasData(
                new City[]
                {
                    new City { Id = 1, CityName = "Kyiv", CountryId = 1, ActiveDataState = true },
                    new City { Id = 2, CityName = "Lviv", CountryId = 1, ActiveDataState = true },
                    new City { Id = 3, CityName = "Odesa", CountryId = 1, ActiveDataState = true },
                    new City { Id = 4, CityName = "Dnipro", CountryId = 1, ActiveDataState = true },
                    new City { Id = 5, CityName = "Kharkiv", CountryId = 1, ActiveDataState = true },

                    new City { Id = 6, CityName = "Washington", CountryId = 2, ActiveDataState = true },
                    new City { Id = 7, CityName = "New York", CountryId = 2, ActiveDataState = true },
                    new City { Id = 8, CityName = "San Francisko", CountryId = 2, ActiveDataState = true },
                    new City { Id = 9, CityName = "Chicago", CountryId = 2, ActiveDataState = true },
                    new City { Id = 10, CityName = "Dallas", CountryId = 2, ActiveDataState = true }
                });

            modelBuilder.Entity<UserRole>().HasData(
                new UserRole[]
                {
                    new UserRole { Id = 1, RoleName = "Guest", ActiveDataState = true },
                    new UserRole { Id = 2, RoleName = "User", ActiveDataState = true },
                    new UserRole { Id = 3, RoleName = "Admin", ActiveDataState = true }
                });


            User user1 = new User { Id = 1, FirstName = "FirstName1", LastName = "LastName1", Login = "Login1", Password = "Password1/*-", Birthday = new DateTime(2001, 01, 01), UserRoleId = 3, CityId = 1, ActiveDataState = true };
            User user2 = new User { Id = 2, FirstName = "FirstName2", LastName = "LastName2", Login = "Login2", Password = "Password2/*-", Birthday = new DateTime(2002, 02, 02), UserRoleId = 2, CityId = 2, ActiveDataState = true };
            List<User> users = new List<User>(new[] { user1, user2 });

            modelBuilder.Entity<User>().HasData(
                new User[]
                {
                    //new User { Id = 1, FirstName = "FirstName1", LastName = "LastName1", Login = "Login1", Password = "Password1", Birthday = new DateTime(2001, 01, 01), UserRoleId = 2, CityId = 1,  ActiveDataState = true },
                    //new User { Id = 2, FirstName = "FirstName2", LastName = "LastName2", Login = "Login2", Password = "Password2", Birthday = new DateTime(2002, 02, 02), UserRoleId = 2, CityId = 2, ActiveDataState = true },
                    users[0],
                    users[1],
                    new User { Id = 3, FirstName = "FirstName3", LastName = "LastName3", Login = "Login3", Password = "Password3/*-+", Birthday = new DateTime(2003, 03, 03), UserRoleId = 2, CityId = 3, ActiveDataState = true }
                });


            //https://stackoverflow.com/questions/15628110/one-to-many-relationship-seeding-in-code-first
            //https://entityframework.net/knowledge-base/8550756/how-to-seed-data-with-many-to-may-relations-in-entity-framework-migrations
        }


        public static void Seed(SsnContext sSNContext)
        {
            sSNContext.Countries.Add(new Country { Id = 1, CountryName = "Ukraine", ActiveDataState = true });
            sSNContext.SaveChanges();
        }



    }
    }

