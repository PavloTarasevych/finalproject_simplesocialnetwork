﻿using System;
using System.Globalization;

namespace SSN.DAL
{
    public class SSNException : Exception
    {
        public string Header { get; }

        public SSNException(string message)
            : base(message)
        {
        }

        public SSNException(string header, string message)
            : base (message)
        {
            Header = header;
        }

        public SSNException(string message, params object[] args)
            : base(String.Format(CultureInfo.CurrentCulture, message, args))
        {
        }

    }
}
