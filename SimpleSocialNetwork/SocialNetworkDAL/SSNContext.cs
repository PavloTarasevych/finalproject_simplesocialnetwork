﻿using Microsoft.EntityFrameworkCore;
using SSN.DAL.Entities;
using System.Collections.Generic;

namespace SSN.DAL
{
    /// <summary>
    /// Context for working with database
    /// </summary>
    public class SsnContext : DbContext
    {
        public DbSet<City> Cities { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserRole> UsersRole { get; set; }

        public SsnContext(DbContextOptions<SsnContext> options)
            : base(options)
        {
            //Database.EnsureDeleted();
            Database.EnsureCreated();
        }

        /// <summary>
        /// Set initial data
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            SsnContextInitializer.Seed(modelBuilder);
        }



    }
}
