﻿using Microsoft.EntityFrameworkCore;
using SSN.DAL.Entities;
using SSN.DAL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SSN.DAL.Repositories
{
    public class CityRepository : IRepository<City>
    {
        private readonly SsnContext _ssncontext;

        public CityRepository(SsnContext snContext)
        {
            _ssncontext = snContext ?? throw new SSNException("CityRepository", "Context is null");
        }

        public Task AddAsync(City item)
        {
            if (null == item)
            {
                throw new SSNException("CityRepository AddAsync", "City item is null");
            }
            return Task.Run(() => _ssncontext.Cities.AddAsync(item));
        }

        public Task DeleteAsync(int id)
        {
            City city = GetById(id);
            if (null == city)
            {
                throw new SSNException("CityRepository DeleteAsync", "City item is null");
            }            
            return Task.Run(() => { city.ActiveDataState = false;
                                    _ssncontext.Entry(city).State = EntityState.Modified; });
        }

        public City GetById(int id)
        {
            return _ssncontext.Cities.Find(id);
        }

        public IEnumerable<City> GetAll()
        {
            return _ssncontext.Cities;
        }

        public void Update(City item)
        {
            if (null == item)
            {
                throw new SSNException("CityRepository Update", "City item is null");
            }
            _ssncontext.Entry(item).State = EntityState.Modified;
        }
    }
}
