﻿using Microsoft.EntityFrameworkCore;
using SSN.DAL.Entities;
using SSN.DAL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SSN.DAL.Repositories
{
    public class PostRepository : IRepository<Post>
    {
        private readonly SsnContext _ssncontext;

        public PostRepository(SsnContext snContext)
        {
            _ssncontext = snContext ?? throw new SSNException("PostRepository", "Context is null");
        }

        public Task AddAsync(Post item)
        {
            if (null == item)
            {
                throw new SSNException("PostRepository AddAsync", "Post item is null");
            }
            return Task.Run(() =>_ssncontext.Posts.AddAsync(item));
        }

        public Task DeleteAsync(int id)
        {
            Post post = GetById(id);
            if (null == post)
            {
                throw new SSNException("PostRepository DeleteAsync", "Post item is null");
            }            
            return Task.Run(() => { post.ActiveDataState = false;
                                    _ssncontext.Entry(post).State = EntityState.Modified; });
        }

        public Post GetById(int id)
        {
            return _ssncontext.Posts.Find(id);
        }

        public IEnumerable<Post> GetAll()
        {
            return _ssncontext.Posts;
        }

        public void Update(Post item)
        {
            if (null == item)
            {
                throw new SSNException("PostRepository Update", "Post item is null");
            }
            _ssncontext.Entry(item).State = EntityState.Modified;
        }
    }
}
