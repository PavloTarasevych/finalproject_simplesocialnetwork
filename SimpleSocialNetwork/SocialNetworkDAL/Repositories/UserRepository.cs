﻿using Microsoft.EntityFrameworkCore;
using SSN.DAL.Entities;
using SSN.DAL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SSN.DAL.Repositories
{
    public class UserRepository : IRepository<User>
    {
        private readonly SsnContext _ssncontext;

        public UserRepository(SsnContext snContext)
        {
            _ssncontext = snContext ?? throw new SSNException("UserRepository", "Context is null");
        }

        public Task AddAsync(User item)
        {
            if (null == item)
            {
                throw new SSNException("UserRepository AddAsync", "User item is null");
            }
            return Task.Run(() => _ssncontext.Users.AddAsync(item));
        }

        public Task DeleteAsync(int id)
        {
            User user = GetById(id);
            if (null == user)
            {
                throw new SSNException("UserRepository DeleteAsync", "User item is null");
            }
            return Task.Run(() => { user.ActiveDataState = false;
                                    _ssncontext.Entry(user).State = EntityState.Modified; });
        }

        public User GetById(int id)
        {
            return _ssncontext.Users.Find(id);
        }

        public IEnumerable<User> GetAll()
        {
            return _ssncontext.Users;
        }

        public void Update(User item)
        {
            if (null == item)
            {
                throw new SSNException("UserRepository Update", "User item is null");
            }
            _ssncontext.Entry(item).State = EntityState.Modified;
        }
    }
}
