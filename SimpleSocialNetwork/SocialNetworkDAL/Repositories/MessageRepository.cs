﻿using Microsoft.EntityFrameworkCore;
using SSN.DAL.Entities;
using SSN.DAL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SSN.DAL.Repositories
{
    public class MessageRepository : IRepository<Message>
    {
        private readonly SsnContext _ssncontext;

        public MessageRepository(SsnContext snContext)
        {
            _ssncontext = snContext ?? throw new SSNException("MessageRepository", "Context is null");
        }

        public Task AddAsync(Message item)
        {
            if (null == item)
            {
                throw new SSNException("MessageRepository AddAsync", "Message item is null");
            }
            return Task.Run(() => _ssncontext.Messages.Add(item));
        }

        public Task DeleteAsync(int id)
        {
            Message message = GetById(id);
            if (null == message)
            {
                throw new SSNException("MessageRepository DeleteAsync", "Message item is null");
            }
            return Task.Run(() => { message.ActiveDataState = false;
                                    _ssncontext.Entry(message).State = EntityState.Modified;});
        }

        public Message GetById(int id)
        {
            return _ssncontext.Messages.Find(id);
        }

        public IEnumerable<Message> GetAll()
        {
            return _ssncontext.Messages;
        }

        public void Update(Message item)
        {
            if (null == item)
            {
                throw new SSNException("MessageRepository Update", "Message item is null");
            }
            _ssncontext.Entry(item).State = EntityState.Modified;
        }
    }
}
