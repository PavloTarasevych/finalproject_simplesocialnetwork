﻿using Microsoft.EntityFrameworkCore;
using SSN.DAL.Entities;
using SSN.DAL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SSN.DAL.Repositories
{
    public class CountryRepository : IRepository<Country>
    {
        private readonly SsnContext _ssncontext;

        public CountryRepository(SsnContext snContext)
        {
            _ssncontext = snContext ?? throw new SSNException("CountryRepository", "Context is null");
        }

        public Task AddAsync(Country item)
        {
            if (null == item)
            {
                throw new SSNException("CountryRepository AddAsync", "City item is null");
            }
            return Task.Run(() => _ssncontext.Countries.AddAsync(item));
        }

        public Task DeleteAsync(int id)
        {
            Country country = GetById(id);
            if (null == country)
            {
                throw new SSNException("CountryRepository DeleteAsync", "Country item is null");
            }           
            return Task.Run(() => { country.ActiveDataState = false;
                                     _ssncontext.Entry(country).State = EntityState.Modified; });
        }

        public Country GetById(int id)
        {
            return _ssncontext.Countries.Find(id);
        }

        public IEnumerable<Country> GetAll()
        {
            return _ssncontext.Countries;
        }

        public void Update(Country item)
        {
            if (null == item)
            {
                throw new SSNException("CountryRepository Update", "Country item is null");
            }
            _ssncontext.Entry(item).State = EntityState.Modified;
        }
    }
}
