﻿using Microsoft.EntityFrameworkCore;
using SSN.DAL.Entities;
using SSN.DAL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SSN.DAL.Repositories
{
    public class UserRoleRepository : IRepository<UserRole>
    {
        private readonly SsnContext _ssncontext;

        public UserRoleRepository(SsnContext snContext)
        {
            _ssncontext = snContext ?? throw new SSNException("UserRoleRepository", "Context is null");
        }

        public Task AddAsync(UserRole item)
        {
            if (null == item)
            {
                throw new SSNException("UserRoleRepository AddAsync", "UserRole item is null");
            }
            return Task.Run(() => _ssncontext.UsersRole.AddAsync(item));
        }

        public Task DeleteAsync(int id)
        {
            UserRole userrole = GetById(id);
            if (null == userrole)
            {
                throw new SSNException("UserRoleRepository DeleteAsync", "UserRole item is null");
            }            
            return Task.Run(() => { userrole.ActiveDataState = false;
                                    _ssncontext.Entry(userrole).State = EntityState.Modified; });
       }

        public UserRole GetById(int id)
        {
            return _ssncontext.UsersRole.Find(id);
        }

        public IEnumerable<UserRole> GetAll()
        {
            return _ssncontext.UsersRole;
        }

        public void Update(UserRole item)
        {
            if (null == item)
            {
                throw new SSNException("UserRoleRepository Update", "UserRole item is null");
            }
            _ssncontext.Entry(item).State = EntityState.Modified;
        }
    }
}
