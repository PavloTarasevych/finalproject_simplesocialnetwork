﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace SSN.DAL.Interfaces
{
    /// <summary>
    /// Main opeartions for repositories
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IRepository<T> where T : class
    {
        IEnumerable<T> GetAll();
        T GetById(int id);
        Task AddAsync(T item);
        void Update(T item);
        Task DeleteAsync(int id);
    }
}
