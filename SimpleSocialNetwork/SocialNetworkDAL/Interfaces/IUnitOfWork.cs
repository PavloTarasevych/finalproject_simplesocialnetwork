﻿using SSN.DAL.Entities;
using System;
using System.Threading.Tasks;

namespace SSN.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<City> CitiesRepository { get; }
        IRepository<Country> CountriesRepository { get; }
        IRepository<Message> MessagesRepository { get; }
        IRepository<Post> PostsRepository { get; }
        IRepository<User> UsersRepository { get; }
        IRepository<UserRole> UserRolesRepository { get; }

        Task SaveAsync();
    }
}
