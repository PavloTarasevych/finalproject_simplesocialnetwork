﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace SSN.DAL
{
    public class SampleContextFactory : IDesignTimeDbContextFactory<SsnContext>
    {
        public SsnContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<SsnContext>();

            ConfigurationBuilder builder = new ConfigurationBuilder();
            builder.SetBasePath(Directory.GetCurrentDirectory());
            builder.AddJsonFile("appsettings.json");
            IConfigurationRoot config = builder.Build();

            string connectionString = config.GetConnectionString("SimpleSocialNetworkDB_connection");
            optionsBuilder.UseSqlServer(connectionString, opts => opts.CommandTimeout((int)TimeSpan.FromMinutes(10).TotalSeconds));
            return new SsnContext(optionsBuilder.Options);
        }
    }
}
