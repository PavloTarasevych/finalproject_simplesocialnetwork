﻿using AutoMapper;
using SSN.BL.Models;
using SSN.DAL.Entities;
using System.Linq;

namespace SSN.BL
{
    public class AutomapperProfileBL : Profile
    {
        public AutomapperProfileBL()
        {
            CreateMap<City, CityModel>()
                .ForMember(cm => cm.Id, opt => opt.MapFrom(c => c.Id))
                .ForMember(cm => cm.CountryId, opt => opt.MapFrom(c => c.CountryId))
                .ForMember(cm => cm.CityName, opt => opt.MapFrom(c => c.CityName))
                .ReverseMap();

            CreateMap<Country, CountryModel>()
                .ForMember(cm => cm.Id, opt => opt.MapFrom(c => c.Id))
                .ForMember(cm => cm.CountryName, opt => opt.MapFrom(c => c.CountryName))
                .ForMember(cm => cm.Cities, opt => opt.MapFrom(c => c.Cities))
                .ReverseMap();

            CreateMap<Post, PostModel>()
                .ForMember(pm => pm.Id, opt => opt.MapFrom(p => p.Id))
                .ForMember(pm => pm.Title, opt => opt.MapFrom(p => p.Title))
                .ForMember(pm => pm.Content, opt => opt.MapFrom(p => p.Content))
                .ForMember(pm => pm.Published, opt => opt.MapFrom(p => p.Published))
                .ForMember(pm => pm.UserIds, opt => opt.MapFrom(p => p.UserIds))
                .ReverseMap();

            CreateMap<Message, MessageModel>()
                .ForMember(mm => mm.Id, opt => opt.MapFrom(m => m.Id))
                .ForMember(mm => mm.Content, opt => opt.MapFrom(m => m.Content))
                .ForMember(mm => mm.DateTimeSent, opt => opt.MapFrom(m => m.DateTimeSent))
                .ForMember(mm => mm.OwnerUserId, opt => opt.MapFrom(m => m.OwnerUserId))
                .ForMember(mm => mm.ToUserId, opt => opt.MapFrom(m => m.ToUserId))
                .ReverseMap();

            CreateMap<User, UserModel>()
                .ForMember(um => um.UserRoleIds, opt => opt.MapFrom(u => u.UserRoleId))
                .ForMember(um => um.CityIds, opt => opt.MapFrom(u => u.CityId))
                .ForMember(um => um.PostsIds, opt => opt.MapFrom(u => u.Posts.Select(p => p.Id)))
                .ForMember(um => um.MessagesIds, opt => opt.MapFrom(u => u.Messages.Select(m => m.Id)))
                .ForMember(um => um.FriendsIds, opt => opt.MapFrom(u => u.Friends.Select(u2 => u2.Id)))
                .ReverseMap();

            CreateMap<UserRole, UserRoleModel>()
                .ForMember(urm => urm.Id, opt => opt.MapFrom(ur => ur.Id))
                .ForMember(urm => urm.RoleName, opt => opt.MapFrom(ur => ur.RoleName))
                .ReverseMap();

        }

    }
}
