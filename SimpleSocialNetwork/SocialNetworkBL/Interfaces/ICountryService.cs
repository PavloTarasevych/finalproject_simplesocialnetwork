﻿using SSN.BL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SSN.BL.Interfaces
{
    public interface ICountryService : ICrud<CountryModel>
    {
    }
}
