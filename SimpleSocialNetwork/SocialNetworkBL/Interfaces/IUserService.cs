﻿using SSN.BL.Models;
using System.Collections.Generic;

namespace SSN.BL.Interfaces
{
    public interface IUserService : ICrud<UserModel>
    {
        UserModel Authenticate(string login, string password);
        bool HasLogin(string login);
        bool VerifyLoginPassword(string login, string password);
        IEnumerable<UserModel> GetByFilter(FilterUserSearchModel filterSearch);
    }
}
