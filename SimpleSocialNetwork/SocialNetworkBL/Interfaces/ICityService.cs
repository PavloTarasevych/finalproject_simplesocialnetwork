﻿using SSN.BL.Models;

namespace SSN.BL.Interfaces
{
    public interface ICityService : ICrud<CityModel>
    {
    }
}
