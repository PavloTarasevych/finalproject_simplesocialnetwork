﻿using SSN.BL.Models;
using System.Collections.Generic;

namespace SSN.BL.Interfaces
{
    public interface IMessageService : ICrud<MessageModel>
    {
        IEnumerable<MessageModel> GetByUserId(int userId);
    }
}
