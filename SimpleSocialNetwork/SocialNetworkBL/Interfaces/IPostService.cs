﻿using SSN.BL.Models;
using System.Collections.Generic;

namespace SSN.BL.Interfaces
{
    public interface IPostService : ICrud<PostModel>
    {
        IEnumerable<PostModel> GetByFilter(FilterPostSearchModel filterSearch);
    }
}
