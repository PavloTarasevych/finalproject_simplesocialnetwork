﻿using SSN.BL.Models;

namespace SSN.BL.Interfaces
{
    public interface IUserRoleService : ICrud<UserRoleModel>
    {
    }
}
