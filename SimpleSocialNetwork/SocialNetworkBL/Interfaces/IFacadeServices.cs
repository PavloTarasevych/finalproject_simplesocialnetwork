﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSN.BL.Interfaces
{
    public interface IFacadeServices
    {
        ICityService CityService { get; }
        ICountryService CountryService { get; }
        IMessageService MessageService { get; }
        IPostService PostService { get; }
        IUserService UserService { get; }
        IUserRoleService UserRoleService { get; }
    }
}
