﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using SSN.BL.Interfaces;
using SSN.BL.Services;
using SSN.DAL;

namespace SSN.BL
{
    /// <summary>
    /// Access fo all services
    /// </summary>
    public class FacadeServices : IFacadeServices
    {
        private readonly string     _connectionString;
        private readonly IMapper    _iMapper;
        private readonly UnitOfWork _unitOfWork;

        private ICountryService _iCountryService;
        private ICityService    _iCityService;
        private IMessageService _iMessageService;
        private IPostService    _iPostService;
        private IUserService    _iUserService;
        private IUserRoleService _iUserRoleService;

        public FacadeServices(string ConnectionString)
        {
            if (string.IsNullOrWhiteSpace(ConnectionString))
            {
                throw new SSNException("AllServices", "ConnectionString is null or empty");
            }
            _connectionString = ConnectionString;
            SsnContext _context = new SsnContext(ContextOptions);
            _iMapper = CreateIMapperProfile();
            _unitOfWork = new UnitOfWork(_context);
        }

        private IMapper CreateIMapperProfile()
        {
            var myProfile = new AutomapperProfileBL();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));

            return configuration.CreateMapper();
        }

        /// <summary>
        /// Get working DBContext
        /// ConnectionString used
        /// </summary>
        private DbContextOptions<SsnContext> ContextOptions
        {
            get
            {
                DbContextOptionsBuilder<SsnContext> optionsBuilder = new DbContextOptionsBuilder<SsnContext>();
                DbContextOptions<SsnContext> options = optionsBuilder
                    .UseSqlServer(_connectionString)
                    .Options;
                return options;
            }
        }

        public ICountryService CountryService 
        {
            get
            {
                if (null == _iCountryService)
                {
                    _iCountryService = new CountryService(_unitOfWork, _iMapper);
                }
                return _iCountryService;
            }
        }

        public ICityService CityService
        {
            get
            {
                if (null == _iCityService)
                {
                    _iCityService = new CityService(_unitOfWork, _iMapper);
                }
                return _iCityService;
            }
        }

        public IMessageService MessageService
        {
            get
            {
                if (null == _iMessageService)
                {
                    _iMessageService = new MessageService(_unitOfWork, _iMapper);
                }
                return _iMessageService;
            }
        }

        public IPostService PostService
        {
            get
            {
                if (null == _iPostService)
                {
                    _iPostService = new PostService(_unitOfWork, _iMapper);
                }
                return _iPostService;
            }
        }

        public IUserService UserService
        {
            get
            {
                if (null == _iUserService)
                {
                    _iUserService = new UserService(_unitOfWork, _iMapper);
                }
                return _iUserService;
            }
        }

        public IUserRoleService UserRoleService
        {
            get
            {
                if (null == _iUserRoleService)
                {
                    _iUserRoleService = new UserRoleService(_unitOfWork, _iMapper);
                }
                return _iUserRoleService;
            }
        }

    }
}
