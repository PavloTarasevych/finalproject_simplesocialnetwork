﻿using AutoMapper;
using SSN.BL.Interfaces;
using SSN.BL.Models;
using SSN.DAL;
using SSN.DAL.Entities;
using SSN.DAL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SSN.BL.Services
{
    public class UserRoleService : IUserRoleService
    {
        private readonly IUnitOfWork _uow;
        private readonly IMapper _iMapper;

        public UserRoleService(IUnitOfWork uow, IMapper imapper)
        {
            _uow = uow ?? throw new SSNException("IUnitOfWork is null", "uow");
            _iMapper = imapper ?? throw new SSNException("Mapper is null", "mapper");
        }

        public Task AddAsync(UserRoleModel model)
        {
            if (null == model ||
                string.IsNullOrWhiteSpace(model.RoleName))
            {
                throw new SSNException("UserRoleModel is null", "model");
            }
            _uow.UserRolesRepository.AddAsync(_iMapper.Map<UserRole>(model));
            return _uow.SaveAsync();
        }

        public Task DeleteByIdAsync(int id)
        {
            UserRole userRole = _uow.UserRolesRepository.GetById(id);
            if (null == userRole)
            {
                throw new SSNException("UserRoleService DeleteByIdAsync", "Role item is null");
            }
            userRole.ActiveDataState = false;
            _uow.UserRolesRepository.Update(userRole);
            return _uow.SaveAsync();
        }

        public IEnumerable<UserRoleModel> GetAll()
        {
            return _iMapper.Map<ICollection<UserRoleModel>>(_uow.UserRolesRepository.GetAll());
        }

        public Task<UserRoleModel> GetByIdAsync(int id)
        {
            return Task.Run(() =>
            {
                UserRole userRole = _uow.UserRolesRepository.GetById(id);
                return _iMapper.Map<UserRoleModel>(userRole);
            });
        }

        public Task UpdateAsync(UserRoleModel model)
        {
            if (null == model)
            {
                throw new SSNException("CityService UpdateAsync", "CityModel is null");
            }
            _uow.UserRolesRepository.Update(_iMapper.Map<UserRole>(model));
            return _uow.SaveAsync();
        }
    }
}
