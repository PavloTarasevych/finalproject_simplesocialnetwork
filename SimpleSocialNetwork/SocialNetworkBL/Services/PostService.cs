﻿using AutoMapper;
using SSN.BL.Interfaces;
using SSN.BL.Models;
using SSN.DAL;
using SSN.DAL.Entities;
using SSN.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SSN.BL.Services
{
    public class PostService : IPostService
    {
        private readonly IUnitOfWork _uow;
        private readonly IMapper _iMapper;

        public PostService(IUnitOfWork uow, IMapper imapper)
        {
            _uow = uow ?? throw new SSNException("IUnitOfWork is null", "uow");
            _iMapper = imapper ?? throw new SSNException("Mapper is null", "mapper");
        }

        public Task AddAsync(PostModel model)
        {
            if (null == model ||
                string.IsNullOrWhiteSpace(model.Title) ||
                string.IsNullOrWhiteSpace(model.Content) ||
                0 > model.UserIds)
            {
                throw new SSNException("PostModel is null", "model");
            }
            _uow.PostsRepository.AddAsync(_iMapper.Map<Post>(model));
            return _uow.SaveAsync();
        }

        public Task DeleteByIdAsync(int id)
        {
            Post post = _uow.PostsRepository.GetById(id);
            if (null == post)
            {
                throw new SSNException("PostService DeleteByIdAsync", "Post item is null");
            }
            post.ActiveDataState = false;
            _uow.PostsRepository.Update(post);
            return _uow.SaveAsync();
        }

        public IEnumerable<PostModel> GetAll()
        {
            return _iMapper.Map<ICollection<PostModel>>(_uow.PostsRepository.GetAll());
        }

        public IEnumerable<PostModel> GetByFilter(FilterPostSearchModel filterSearch)
        {
            throw new NotImplementedException();
        }

        public Task<PostModel> GetByIdAsync(int id)
        {
            return Task.Run(() =>
            {
                Post post = _uow.PostsRepository.GetById(id);
                return _iMapper.Map<PostModel>(post);
            });
        }

        public Task UpdateAsync(PostModel model)
        {
            if (null == model)
            {
                throw new SSNException("PostService UpdateAsync", "PostModel is null");
            }
            _uow.PostsRepository.Update(_iMapper.Map<Post>(model));
            return _uow.SaveAsync();
        }
    }
}
