﻿using AutoMapper;
using SSN.BL.Interfaces;
using SSN.BL.Models;
using SSN.DAL;
using SSN.DAL.Entities;
using SSN.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSN.BL.Services
{
    public class MessageService : IMessageService
    {
        private readonly IUnitOfWork _uow;
        private readonly IMapper _iMapper;

        public MessageService(IUnitOfWork uow, IMapper imapper)
        {
            _uow = uow ?? throw new SSNException("IUnitOfWork is null", "uow");
            _iMapper = imapper ?? throw new SSNException("Mapper is null", "mapper");
        }

        public Task AddAsync(MessageModel model)
        {
            if (null == model ||
                string.IsNullOrWhiteSpace(model.Content) ||
                0 > model.OwnerUserId ||
                0 > model.ToUserId)
            {
                throw new SSNException("MessageModel is null", "model");
            }
            _uow.MessagesRepository.AddAsync(_iMapper.Map<Message>(model));
            return _uow.SaveAsync();
        }

        public Task DeleteByIdAsync(int id)
        {
            Message message = _uow.MessagesRepository.GetById(id);
            if (null == message)
            {
                throw new SSNException("MessageService DeleteByIdAsync", "Message item is null");
            }
            message.ActiveDataState = false;
            _uow.MessagesRepository.Update(message);
            return _uow.SaveAsync();
        }

        public IEnumerable<MessageModel> GetAll()
        {
            return _iMapper.Map<ICollection<MessageModel>>(_uow.MessagesRepository.GetAll());
        }

        public Task<MessageModel> GetByIdAsync(int id)
        {
            return Task.Run(() =>
            {
                Message message = _uow.MessagesRepository.GetById(id);
                return _iMapper.Map<MessageModel>(message);
            });
        }

        public IEnumerable<MessageModel> GetByUserId(int userId)
        {
            ICollection<MessageModel> messages = _iMapper.Map<ICollection<MessageModel>>(_uow.MessagesRepository.GetAll().Select(m => m).Where(m => m.OwnerUserId == userId));
            return messages;
        }

        public Task UpdateAsync(MessageModel model)
        {
            if (null == model)
            {
                throw new SSNException("MessageService UpdateAsync", "MessageModel is null");
            }
            _uow.MessagesRepository.Update(_iMapper.Map<Message>(model));
            return _uow.SaveAsync();
        }
    }
}
