﻿using AutoMapper;
using SSN.BL.Interfaces;
using SSN.BL.Models;
using SSN.DAL;
using SSN.DAL.Entities;
using SSN.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SSN.BL.Services
{
    public class CountryService : ICountryService
    {
        private readonly IUnitOfWork _uow;
        private readonly IMapper _iMapper;

        public CountryService(IUnitOfWork uow, IMapper imapper)
        {
            _uow = uow ?? throw new SSNException("IUnitOfWork is null", "uow");
            _iMapper = imapper ?? throw new SSNException("Mapper is null", "mapper");
        }

        public Task AddAsync(CountryModel model)
        {
            if (null == model ||
                string.IsNullOrWhiteSpace(model.CountryName))
            {
                throw new SSNException("CountryModel data is wrong", "model");
            }
            _uow.CountriesRepository.AddAsync(_iMapper.Map<Country>(model));
            return _uow.SaveAsync();
        }

        public Task DeleteByIdAsync(int id)
        {
            Country country = _uow.CountriesRepository.GetById(id);
            if (null == country)
            {
                throw new SSNException("CountryService DeleteByIdAsync", "Country item is null");
            }
            country.ActiveDataState = false;
            _uow.CountriesRepository.Update(country);
            return _uow.SaveAsync();
        }

        public IEnumerable<CountryModel> GetAll()
        {
            return _iMapper.Map<ICollection<CountryModel>>(_uow.CountriesRepository.GetAll());
        }

        public Task<CountryModel> GetByIdAsync(int id)
        {
            return Task.Run(() =>
            {
                Country country = _uow.CountriesRepository.GetById(id);
                return _iMapper.Map<CountryModel>(country);
            });
        }

        public Task UpdateAsync(CountryModel model)
        {
            if (null == model)
            {
                throw new SSNException("CountryService UpdateAsync", "CountryModel is null");
            }
            _uow.CountriesRepository.Update(_iMapper.Map<Country>(model));
            return _uow.SaveAsync();
        }
    }
}
