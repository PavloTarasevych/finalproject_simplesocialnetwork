﻿using AutoMapper;
using SSN.BL.Interfaces;
using SSN.BL.Models;
using SSN.DAL;
using SSN.DAL.Entities;
using SSN.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSN.BL.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _uow;
        private readonly IMapper _iMapper;

        public UserService(IUnitOfWork uow, IMapper imapper)
        {
            _uow = uow ?? throw new SSNException("IUnitOfWork is null", "uow");
            _iMapper = imapper ?? throw new SSNException("Mapper is null", "mapper");
        }

        public async Task AddAsync(UserModel model)
        {
            if (null == model ||
                string.IsNullOrWhiteSpace(model.FirstName) ||
                string.IsNullOrWhiteSpace(model.LastName) ||
                string.IsNullOrWhiteSpace(model.Login) ||
                string.IsNullOrWhiteSpace(model.Password))
            {
                throw new SSNException("UserModel is null", "model");
            }
            model.UserRoleIds = 2;
            await _uow.UsersRepository.AddAsync(_iMapper.Map<User>(model));
            await _uow.SaveAsync();
        }

        public async Task DeleteByIdAsync(int id)
        {
            User user = _uow.UsersRepository.GetById(id);
            if (null == user)
            {
                throw new SSNException("UserService DeleteByIdAsync", "User item is null");
            }
            user.ActiveDataState = false;
            _uow.UsersRepository.Update(user);
            await _uow.SaveAsync();
        }

        public IEnumerable<UserModel> GetAll()
        {
            return _iMapper.Map<ICollection<UserModel>>(_uow.UsersRepository.GetAll());
        }

        public IEnumerable<UserModel> GetByFilter(FilterUserSearchModel filterSearch)
        {
            throw new NotImplementedException();
        }

        public Task<UserModel> GetByIdAsync(int id)
        {
            return Task.Run(() =>
            {
                User user = _uow.UsersRepository.GetById(id);
                return _iMapper.Map<UserModel>(user);
            });
        }

        public async Task UpdateAsync(UserModel model)
        {
            if (null == model)
            {
                throw new SSNException("UserService UpdateAsync", "UserModel is null");
            }
            _uow.UsersRepository.Update(_iMapper.Map<User>(model));
            await _uow.SaveAsync();
        }

        public bool HasLogin(string login)
        {
            if (string.IsNullOrWhiteSpace(login))
            {
                return false;
            }
            User user = _uow.UsersRepository.GetAll().Select(u => u).Where(u => u.Login == login).FirstOrDefault();
            return null != user;
        }

        public bool VerifyLoginPassword(string login, string password)
        {
            if (string.IsNullOrWhiteSpace(login) || string.IsNullOrWhiteSpace(password))
            {
                return false;
            }
            User user = _uow.UsersRepository.GetAll().Select(u => u).Where(u => u.Login == login && u.Password == password).FirstOrDefault();
            return null != user;
        }

        public UserModel Authenticate(string login, string password)
        {
            if (string.IsNullOrWhiteSpace(login) || string.IsNullOrWhiteSpace(password))
            {
                return null;
            }
            User user = _uow.UsersRepository.GetAll().FirstOrDefault(u => u.Login == login && u.Password == password);
            return _iMapper.Map<UserModel>(user);
        }
    }
}
