﻿using AutoMapper;
using SSN.BL.Interfaces;
using SSN.BL.Models;
using SSN.DAL;
using SSN.DAL.Entities;
using SSN.DAL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SSN.BL.Services
{
    public class CityService : ICityService
    {
        private readonly IUnitOfWork _uow;
        private readonly IMapper _iMapper;

        public CityService(IUnitOfWork uow, IMapper imapper)
        {
            _uow = uow ?? throw new SSNException("IUnitOfWork is null", "uow");
            _iMapper = imapper ?? throw new SSNException("Mapper is null", "mapper");
        }

        public Task AddAsync(CityModel model)
        {
            if (null == model || 
                string.IsNullOrWhiteSpace(model.CityName) || 
                0 > model.CountryId)
            {
                throw new SSNException("CityModel is null", "model");
            }
            _uow.CitiesRepository.AddAsync(_iMapper.Map<City>(model));
            return _uow.SaveAsync();
        }

        public Task DeleteByIdAsync(int id)
        {
            City city = _uow.CitiesRepository.GetById(id);
            if (null == city)
            {
                throw new SSNException("CityService DeleteByIdAsync", "City item is null");
            }
            city.ActiveDataState = false;
            _uow.CitiesRepository.Update(city);
            return _uow.SaveAsync();
        }

        public IEnumerable<CityModel> GetAll()
        {
            return _iMapper.Map<ICollection<CityModel>>(_uow.CitiesRepository.GetAll());
        }

        public Task<CityModel> GetByIdAsync(int id)
        {
            return Task.Run(() =>
            {
                City city = _uow.CitiesRepository.GetById(id);
                return _iMapper.Map<CityModel>(city);
            });
        }

        public Task UpdateAsync(CityModel model)
        {
            if (null == model)
            {
                throw new SSNException("CityService UpdateAsync", "CityModel is null");
            }
            _uow.CitiesRepository.Update(_iMapper.Map<City>(model));
            return _uow.SaveAsync();
        }
    }
}
