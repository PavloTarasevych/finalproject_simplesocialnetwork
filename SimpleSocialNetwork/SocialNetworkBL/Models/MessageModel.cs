﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SSN.BL.Models
{
    public class MessageModel
    {
        [Required]
        [Range(0, int.MaxValue)]
        public int Id { get; set; }
        [Required]
        [StringLength(int.MaxValue, MinimumLength = 1, ErrorMessage = "{0} length must be between {2} and {1}.")]
        public string Content { get; set; }
        public DateTime DateTimeSent { get; set; }

        [Required]
        [Range(0, int.MaxValue)]
        public int OwnerUserId { get; set; }
        [Required]
        [Range(0, int.MaxValue)]
        public int ToUserId { get; set; }
    }
}
