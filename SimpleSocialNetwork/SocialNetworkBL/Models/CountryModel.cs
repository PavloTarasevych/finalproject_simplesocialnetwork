﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SSN.BL.Models

{
    public class CountryModel
    {
        [Required]
        [Range(0, int.MaxValue)]
        public int Id { get; set; }
        [Required]
        [StringLength(100, MinimumLength = 2, ErrorMessage = "{0} length must be between {2} and {1}.")]
        public string CountryName { get; set; }

        public ICollection<CityModel> Cities { get; set; }
    }
}
