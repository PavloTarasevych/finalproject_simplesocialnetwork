﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SSN.BL.Models
{
    public class UserModel
    {
        [Required]
        [Range(0, int.MaxValue)]
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 1, ErrorMessage = "{0} length must be between {2} and {1}.")]
        //[Remote(action: "HasLogin", controller: "Users", ErrorMessage = "Login already exist")]
        public string Login { get; set; }
        [Required]
        [StringLength(40, MinimumLength = 5, ErrorMessage = "{0} length must be between {2} and {1}.")]
        [RegularExpression(@"(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#/*-+]).{5,40}")]
        [DataType(DataType.Password)]
        //[Remote(action: "VerifyLoginPassword", controller: "Users", HttpMethod = "POST", ErrorMessage = "Wrong Login and/or Password")]
        public string Password { get; set; }

        public DateTime Birthday { get; set; }

        [Required]
        public int UserRoleIds { get; set; }
        public int CityIds { get; set; }
        public ICollection<int> PostsIds { get; set; }
        public ICollection<int> MessagesIds { get; set; }
        public ICollection<int> FriendsIds { get; set; }
    }
}
