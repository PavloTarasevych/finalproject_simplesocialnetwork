﻿using System.ComponentModel.DataAnnotations;

namespace SSN.BL.Models
{
    public class UserRoleModel
    {
        [Required]
        [Range(0, int.MaxValue)]
        public int Id { get; set; }
        [Required]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "{0} length must be between {2} and {1}.")]
        public string RoleName { get; set; }
    }
}
