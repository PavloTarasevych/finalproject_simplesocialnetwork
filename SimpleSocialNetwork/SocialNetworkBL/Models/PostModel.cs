﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SSN.BL.Models
{
    public class PostModel
    {
        [Required]
        [Range(0, int.MaxValue)]
        public int Id { get; set; }
        [Required]
        [StringLength(200, MinimumLength = 1, ErrorMessage = "{0} length must be between {2} and {1}.")]
        public string Title { get; set; }
        [Required]
        [StringLength(int.MaxValue, MinimumLength = 1, ErrorMessage = "{0} length must be between {2} and {1}.")]
        public string Content { get; set; }
        [Required]
        public DateTime Published { get; set; }


        [Required]
        [Range(0, int.MaxValue)]
        public int UserIds { get; set; }
    }
}
