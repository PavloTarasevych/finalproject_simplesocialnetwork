﻿namespace SSN.BL.Models
{
    public class FilterUserSearchModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int FromDate { get; set; }
        public int ToDate { get; set; }
    }
}
